#pragma once

#include <vector>

#include "chronoGPU.hpp"
#include "chronoCPU.hpp"

namespace IMAC
{
	__global__
    void rgb_to_hsv(const uchar3* const input, const int imgWidth, const int imgHeight, uchar3* const output);
	__global__
	void hsv_to_rgb(const uchar3* const input, const int imgWidth, const int imgHeight, uchar3* const output);
    __global__
    void compute_histogram(const uchar3* const input, const int imgWidth, const int imgHeight, int* const dev_histogram);
    __global__
    void compute_repartition(const int* const dev_histogram, int* const result);
    __global__
    void equalize_histogram(const uchar3* const input, const int imgWidth, const int imgHeight, const int* const r, const uchar3* const output);
    
    void histogramGPU(const std::vector<uchar3> &inputImg, const uint imgWidth, const uint imgHeight, std::vector<uchar3> &output);

    void printTiming(const float2 timing);
}